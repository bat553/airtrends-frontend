<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 20/05/2018
 * Time: 14:06
 * Main Page
 */
$page = 'index';
require_once './src/functions.php';
require_once './src/head.php';
?>
<?php
// T/A of the Week
include  'src/t_a_of_week.php';
?>

<?php
// Top
include 'src/index-top-array.php';
?>

<?php
// Pooler Status
include 'src/pooler-status.php';
?>

<?php
require_once 'src/footer.php';
