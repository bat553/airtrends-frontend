<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 10/06/2018
 * Time: 16:28
 */
$title = "User Profile";
require_once "src/functions.php";

if (!isset($_SESSION['name'])){
    header('Location: ./login.php');
    exit();
}

// HEAD
require_once "src/head.php";

// Prez
require_once "src/profile/prez.php";


// Footer
require_once "src/footer.php";



