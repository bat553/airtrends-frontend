This is AirTrends Source Code.
This code is private and should not be shared with anyone.

Let's decompose the code !
    Every file ended with "dex.php" is an "index" file.
        "index" files are the files that include php file to create the final result
    Files in the "include" folder are scripts called by the index. 
        Each file do a single task ex.: "footer.php" -> Print the footer of the page
   
   This type of structure make the frontend easily upgradable.
    
   "oauth" folder make the oauth requests to the music providers
   
   "search" folder is used to use the search function with a better uri
   
Variable nomenclature :
    Start with :
       f = Function
       o = Object
       s = String (string, array...)
       b = Boolean 
       n = Numeric 
       
   Contain : 
       Sp = Spotify 
       Dz = Deezer 
       Art = ID externe de l'artiste
   
   Usefull Variables:
       $track_id = Identifiant interne du morceau 
       $artist_id = Identifient intene de l'artiste
       $sRadio = Nom de la radio
       $a,b,c = Temp digits 
       $sType = Type (artiste,morceau,radio)
       $sKeyWords = Mot clée(s) si c'est une recherche
       $sTitle = Titre de la page (utilisé dans head.php)
       $sDesc = Description de la page (utilisé dans head.php)
       
Function nomenclature :
    Start with :
        get = Get a value in an Object 
        set = Set the object value 
        