Variables list

#  functions.php 
#### Contient toutes les fonctions du frontend
## Orpheline 
$sVersion = String : Version du frontend (vX.X)

## Test Maintencance
$bMaintenance = Boolean : Maintenance forcée on/off

$sE = String : Message d'exception

$sMsg = String : Réponse des deux test effectués (Connectivitée et Elasticearch)

## Elasticsearch
$oClient = Object : Faire des requetes vers elasticsearch

## Deezer
$oDzApi = Object : Interaction avec Deezer

## Spotify
$sApApiKey = String : Access token de Spotify

$sHeader = String : Header spotify (Bearer)

## MySQL
$oBDD = Object : Interaction avec la BDD


### Elasticsearch based functions
$sJson = String : Requete à effectuée en Json

$sParams = String : Requete finale utilisée par l'api (Array)

$sResult = String : Réponse de ElasticSearch en Array

### Curl based function
$oCh = Object : Curl function

$oResponse = Object : Curl response (text,httpcode...)

### MySQL based functions
$oReq = Object : Requete vers la BDD

$sResult = String : Réponse sous forme de tableau 

# artiste-class.php 
#### Affiche la position des artistes dans le classement 
$bFound = Boolean : Si le morceau est dans le classement

$dClassement = Digit : Place dans le classement (Permet re retier des entrée si le morceau est premier ou dernier)

$sArray_classement = String : Id de l'artiste dans un tableau (0 = en cours, 3-1 = les artistes en dessous, 4-6 = les artistes au dessus)

# current-play.php
#### Affiche les morceau en cours de diffusion dans un tableau
$sCurrentPlay = Tableau avec les infos des derniers morceaux crawlés

# footer.php
#### Footer de toutes les pages, il permet de récupérer et d'envoyer les infos des utilisateurs sur elasticsearch
$dEndTime = Digit : Temps à la fin de la génération de la page

$dStartTime = Digit : Temps au début de la génération de la page

$dLoadTime = Digit : Soustration des deux var précédentes = Temps de génération de la page

$oMe = Object : Contient les informations de l'utilisateur d'un provider externe (Spotify/Deezer)

$sUser = Nom d'utilisateur si connecté, sinon nom d'utilisateur = "guest"

$sKeyWords = String : Mot clée(s) si c'est une recherche

# head.php
#### Head de toute les pages 
$sButton = String : Affiche le menu de login selon le provider


# info-artiste.php
#### Affiche et met en page les infos des artistes 
$sInfos = String : Infos sous forme de tableau

$sImg = String : Lien vers la jackette de l'artiste 

$sTop5K = String : Classement des 5000 artistes les plus diffusés

$sClassement = String : Position du morceau dans le classement 

$sArray_classement = String : Position de l'artiste par rapport aux 3 du dessus et du dessous

$bFound = Boolean : L'artiste ce trouve dans les 5000 premiers ?

$sNote = String : Note de l'artiste 

$sDiffuseurs = String : Liste des radio diffusant cet artiste

$sTopTracks = String : Liste des morceaux les plus diffusés de l'artiste

# info-song.php
#### Affiche et met en page les infos des artistes 
$sInfos = String : Infos sous forme de tableau

$sFirstradio / $sFirstPlayed = String : Date et radio de la premiere diffusion 

$sImg = String : Lien vers la jackette du titre 

$sTop5K = String : Classement des 5000 morceaux les plus diffusés

$sClassement = String : Position du morceau dans le classement 

$sArray_classement = String : Position de l'artiste par rapport aux 3 du dessus et du dessous

$bFound = Boolean : Le morceau ce trouve dans les 5000 premiers ?

$sNote = String : Note du morceau 

$sCover = String : Code iframe des lecteurs deezers et spotify

# search.php
#### Effectue une recherche et met en page le résultat
$sQuery = String : Query de l'utilisateur

# top-array.php
#### Affiche les tableaux avec les trends
$sTop_15 = String : Top 15 des morceaux les plus joués (elasticsearch)

$sTrend = String : Contient les trends des 2 dernieres semaines

$sTrending = String : Trending de la semaine derniere

$sTrendingMinus1 = String : trending d'il y a 3 semaines 

# radiodex.php
#### Affiche et met en page les profils des radios
$sRadio = String : Nom de la radio

$sInfos = String : Infos en tout genre

$sTrending = String : Track trending on the radio

$sTrendingArt = String : Artist trending on the radio

$sNote = String : Note de la radio

$sLastPlayed = String : Dernier morceaux joués par la radio





