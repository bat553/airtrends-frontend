<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 10/06/2018
 * Time: 16:46
 */
require_once './src/functions.php';
$dStartTime = getLoadTime();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Login | AirTrends </title>


    <link rel="apple-touch-icon" sizes="180x180" href="https://static.onairtrends.org/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://static.onairtrends.org/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://static.onairtrends.org/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://static.onairtrends.org/favicon/site.webmanifest">
    <link rel="shortcut icon" href="https://static.onairtrends.org/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="https://static.onairtrends.org/favicon/browserconfig.xml">
    <meta name="theme-color" content="#11C0E9">


    <!-- Slim CSS -->
    <link rel="stylesheet" href="https://static.onairtrends.org/css/slim.min.css">

<div class="signin-wrapper air-background">

    <div class="signin-box">
        <h2 class="airtrends-logo-color" style="margin-bottom: 30%">
            <a href="https://onairtrends.org/index.php">AIRTRENDS</a>
        </h2>
        <h2 class="signin-title-primary">Welcome back!</h2>
        <h3 class="signin-title-secondary">Sign in to continue.</h3>

        <div class="container">
            <a href="./oauth/spotify.php">
                <button class="btn btn-oblong btn-outline-primary btn-block">Spotify</button>
            </a>
        </div>
        <br>
        <div class="container">
            <a href="./oauth/deezer.php">
                <button class="btn btn-oblong btn-outline-primary btn-block">Deezer</button>
            </a>
        </div>
    </div>

</div>

<?php
    require_once './src/footer.php';