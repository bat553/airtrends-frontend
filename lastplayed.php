<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 02/06/2018
 * Time: 12:35
 */
// Track timeline
require_once './src/functions.php';
$title = 'Last Pooled Tracks';
require_once './src/head.php';

function getLastPlayed(){
    global $oClient;

    $sJson = '{
          "size": 15,
          "sort": [
            {
              "timestamp": {
                "order": "desc"
              }
            }
          ]
        }';

    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];

    return $oClient->search($sParams)['hits']['hits'];
}
$lasts = getLastPlayed();

?>
<div class="slim-pageheader"></div>
    <div class="row row-sm row-timeline">
        <div class="col-lg-12">
            <div class="card pd-30">
                <div class="timeline-group">
                    <?php
                    $a = 0;
                    foreach ($lasts as $last){
                        $infos = getTrackID($last['_source']['track_id']);
                        $track_id = $last['_source']['track_id'];

                        $track = $infos['track'];
                        $artist = $infos['artist'];
                        $artist_id = $infos['artist_id'];

                        $date = new DateTime($last['_source']['timestamp']);
                        $timestamp = $date->setTimezone(new DateTimeZone('Europe/Paris'));
                        $timestampH = $date->format('H');
                        $timestampi = $date->format('i');
                        $timestamp = $timestampH.'h'.$timestampi;
                        $station_id = $last['_source']['station_id'];
                        $station = getRadioInfos($station_id);

                        if ($a == 0){
                            $class = "timeline-day";
                            $pin = "&nbsp;";
                            $timestamp = "Latest";
                        }
                        else{
                            $class = "";
                            $pin = "";
                        }


                        print '                    
                            <div class="timeline-item '.$class.'">
                                <div class="timeline-time">'.$timestamp.'</div>
                                <div class="timeline-body">
                                    <p class="timeline-title"><a href="./items.php?track_id='.$track_id.'">'.$track.'</a></p>
                                    <p class="timeline-author"><a href="./items.php?artist_id='.$artist_id.'">'.$artist.'</a> On <a href="./items.php?station_id='.$station_id.'">'.$station['display_name'].'</a> </p>
                                </div>
                            </div>';
                        ++$a;
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
require_once './src/footer.php';
