<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 27/05/2018
 * Time: 16:17
 */
// Swano Corp 2018 | Search engine

require_once '../src/functions.php';
require_once '../src/head.php';
?>
    <div class="slim-pageheader">
        <ol class="breadcrumb slim-breadcrumb"></ol>
        <h6 class="slim-pagetitle air-text-clear">Search Result</h6>
    </div>
<div class="section-wrapper">
<?php


print('<div class="container">');
echo "<table class='table'>";

if ($_GET['artist'] or $_GET['track'] or $_GET['q']){

    if ($_GET['artist']){
        $type = "artist";
        $query = htmlspecialchars(strtolower($_GET['artist']));
    }
    elseif ($_GET['track']){
        $type = "track";
        $query = htmlspecialchars(strtolower($_GET['track']));
    }
    else {
        $type = "string";
        $query = htmlspecialchars(strtolower($_GET['q']));
    }

function search($type, $query)
{
    global $oClient;
    $sJson = '{
          "query": {
            "match": {
              "' . $type . '": {
                "query": "' . $query . '", 
                "operator": "and",
                "fuzziness": 1.75
              }
            }
          },
          "size" : 15
        }';

    if($type == "artist"){
        $sJson = '{
          "query": {
            "match": {
              "' . $type . '": {
                "query": "' . $query . '", 
                "operator": "and",
                "fuzziness": 1.75
              }
            }
          },
          "size" : 0,
            "aggs": {
                "_doc": {
                  "terms": {
                    "field": "artist_id",
                    "size": 10
                  }
                }
              }
        }';
    }

    $sParams = [
        'index' => 'air-link',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];

    return $oClient->search($sParams);


}
$sKeyWords = $query;

    $sResult = search($type, $query);

    if (!$sResult['hits']['total'] and $type != "artist")  $sResult = search('string', $query);  // Si pas de résultat alors essayé avec le string

    if (!$sResult['hits']['total']) echo "<th>Item not found... Come check later !</th>";
    elseif ($type == "track" or  $type == "string"){
        echo "<tr><div class='text-right'>".$sResult['hits']['total']." result(s) found(s) in ".$sResult['took']."ms </div></tr>";
        echo "<tr><th>Artist</th><th>Track</th></tr>";
        $sResult = $sResult['hits']['hits'];
        $a = 0;
        foreach ($sResult as $result){
            $artist = $result['_source']['artist'];
            $track = $result['_source']['track'];
            $track_id = $result['_source']['track_id'];
            $artist_id = $result['_source']['artist_id'];
            echo "<tr><th><a href='https://onairtrends.org/items.php?artist_id=".$artist_id."'>". $artist ."</a></th><th><a href='/items.php?track_id=".$track_id."'>". $track ."</a></th></tr>";
            ++$a;
        }

    }
    elseif ($type == "artist" ){
        echo "<tr><div class='text-right'>".$sResult['hits']['total']." result(s) found(s) in ".$sResult['took']."ms </div></tr>";
        $sResult = $sResult['aggregations']['_doc']['buckets'];
        //$sResult = array_unique($sResult['hits']['hits']);
        $a = 0;
        foreach ($sResult as $result){
            $artist_id = $result['key'];
            $infos = getArtistID($artist_id);
            $cover = getDzArtist($infos['d_artist_id'])['picture'];
            echo "<tr><th class='text-center'><a href='https://onairtrends.org/items.php?artist_id=".$artist_id."'><img alt='". $infos['artist'] ."' width='150px' class='rounded-circle img-thumbnail' src='".$cover."'><br><h2>". $infos['artist'] ."</h2></a></th></tr>";
            ++$a;
        }
    }


    else {
        print "<h1> Error ! </h1>";
    }

    echo "</table>";

}
else {
    echo "</table>";
}
print('</div></div>');

require_once '../src/footer.php';
?>

