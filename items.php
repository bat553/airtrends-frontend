<?php
// Swano Corp 2018
// Front affichage items

// todo : HistoGraph pour les items (heures de passage et heures de diffusion)
// todo : Graph progression
if(isset($_GET['artist_id'])){
    $artist_id = htmlspecialchars($_GET['artist_id']);
    $type = 'artist_id';
    $type2 = 'artist';
    require_once './src/functions.php';

    $infos = getArtistID($artist_id);
    get404($infos);

    $prog = getProgress($type, $artist_id);
    $cover = getDzArtist($infos['d_artist_id'])['picture_medium'];
    $stats = getItemStats($type, $artist_id);
    $stations_stats = getArtistTopStation($artist_id);
    $top_tracks = getArtistTopTracks($artist_id);

    $oTop5K = getGlobalRank($type, $artist_id);

    $title = $infos['artist'];
    require_once './src/head.php';

    require_once './src/items/prez.php';
    require_once './src/items/rank-items.php';
    require_once './src/items/artist-tt.php';
    require_once './src/footer.php';
}
elseif (isset($_GET['track_id'])){
    $track_id = htmlspecialchars($_GET['track_id']);
    require_once './src/functions.php';

    $type = 'track_id';
    $type2 = 'track';
    $infos = getTrackID($track_id);
    get404($infos);

    $prog = getProgress($type, $track_id);
    $cover = getDzTrack($infos['d_track_id'])["album"]['cover_medium'];
    $stats = getItemStats($type, $track_id);
    $stations_stats = getTrackTopStation($track_id);
    $col_prez = "col-xl-12";


    $oTop5K = getGlobalRank($type, $track_id);

    $title = $infos['track'];
    require_once './src/head.php';

    require_once './src/items/prez.php';
    require_once './src/items/rank-items.php';
    require_once './src/footer.php';
}
elseif (isset($_GET['station_id'])){
    $station_id = htmlspecialchars($_GET['station_id']);
    require_once './src/functions.php';
    $infos = getRadioInfos($station_id);
    get404($infos);


    $cover = $infos['picurl'];
    $type = 'station_id';
    $note = getRadioNote($station_id);
    $stats = getItemStats($type, $station_id);
    $station_tracks = getTrendingStation('track', $station_id);
    $station_artists = getTrendingStation('artist', $station_id);
    $col_prez = "col-xl-7";
    $last_tracks = getLastPlayedStation($station_id);

    $title = $infos['display_name'];
    require_once './src/head.php';

    require_once './src/items/prez.php';
    require_once './src/items/station-ranks.php';
    require_once './src/items/station-tt.php';
    require_once './src/footer.php';
}
else {
    http_response_code(404);
    include '../errors/404.html';
}
