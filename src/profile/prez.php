<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 10/06/2018
 * Time: 16:31
 */

?>

<div class="slim-pageheader">
    <div class="slim-pagetitle text-white">Profile</div>
</div>
<div class="section-wrapper">
    <div class="row row-sm" >
        <div class="col-lg-8" >
            <div class="card card-profile" >
                <div class="card-body" >
                    <div class="media">
                        <img src="<?=getProfilePic($_SESSION['type'])?>" alt="">
                        <div class="media-body">
                            <h3 class="card-profile-name"><?=$_SESSION['name']?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="alert alert-warning"><strong>Page under development! </strong> Useless for the moment...</div>
        </div>
    </div>
</div>
