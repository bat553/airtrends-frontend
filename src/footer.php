<?php
$dEndTime = getLoadTime();
$dLoadTime = round(($dEndTime  - $dStartTime), 4);

// Logs
if(isset($_SESSION['name'])) { $user = $_SESSION['name']; }
if(!isset($keyword)){$keyword = NULL;}
if(!isset($_SERVER["HTTP_REFERER"])){$_SERVER["HTTP_REFERER"] = NULL;}
setLogElastic(array(date('y-m-d H:i:s'),  $_SERVER["REQUEST_URI"], $dLoadTime, $keyword, $_SERVER["REMOTE_ADDR"], $_SERVER["HTTP_USER_AGENT"], $_SERVER["HTTP_REFERER"], IsLogged()[1], $user));
// END
?>
</div>
</div>
<div class="slim-pageheader"></div>
<footer class="footer footer-background" >
    <div class="text-capitalize text-center text-white">
        <h6>Made with <i class="fas fa-heart"></i> by <a class="text-white" href="https://swano-lab.net">Swano</a> since 2017</h6>
        <span class="text-muted text-right">Load time : <?=$dLoadTime?></span>
    </div>
</footer>
</div>



<!-- Custom CSS -->
<link rel="stylesheet" href="https://static.onairtrends.org/css/custom.min.css">

<!--Vendor CSS-->
<link href="https://static.onairtrends.org/lib/select2/css/select2.min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">



<script src="https://static.onairtrends.org/lib/jquery/js/jquery.min.js"></script>
<script src="https://static.onairtrends.org/lib/popper.js/js/popper.min.js"></script>
<script src="https://static.onairtrends.org/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="https://static.onairtrends.org/lib/select2/js/select2.full.min.js"></script>
<script src="https://static.onairtrends.org/lib/raphael/js/raphael.min.js"></script>
<script src="https://static.onairtrends.org/lib/morris.js/js/morris.min.js"></script>
<script src="https://static.onairtrends.org/js/select-dropdown.js"></script>

<?php if (isset ($page) && $page = 'index') {
    print ('<script src="https://static.onairtrends.org/js/app-infos-3.js" ></script>');}
?>


</body>
</html>
