<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 20/05/2018
 * Time: 17:35
 */
// Top Array index.php
// Trending artists
$sTrend_a = GetTrending('artist_id');
$sTrending_a = $sTrend_a[0];
$sTrendingMinus1_a = $sTrend_a[1];

//Trending tracks
$sTrend_t = GetTrending('track_id');
$sTrending_t = $sTrend_t[0][0];
$sTrendingMinus1_t = $sTrend_t[1];


?>



<div class="slim-pageheader">
    <ol class="breadcrumb slim-breadcrumb"></ol>
    <h6 class="slim-pagetitle air-text-clear">Weekly rank</h6>
</div>
<div class="section-wrapper">
    <div class="row">
        <div class="col-xl-6">
            <div class="container">
                <div class="card-title">Top Tracks</div>
                <table class="table">
                    <tbody>
                    <?php
                    $a = 0;
                    while ($a < 10){
                        $track_id = $sTrend_t[0][$a]['key'];
                        $dPlay = $sTrend_t[$a]['doc_count'];
                        $infos = getTrackID($track_id);
                        $artist = $infos['artist'];
                        $track = $infos['track'];
                        $artist_id = $infos['artist_id'];
                        $updown = getUpDown($a, $track_id, $sTrendingMinus1_t);
                        ++$a;
                        $class = getClassRank($a);
                        print('
                    <tr class="'.$class[0].'">
                        <th>
                            <span class="'.$class[1].'"> '.$a.' </span>
                        </th>
                        <th>
                            <a href="./items.php?track_id='.$track_id.'">'.$track.' </a>
                                <a class="text-capitalize text-muted" href="./items.php?artist_id='.$artist_id.'">'.$artist.'</a>
                        </th>
                        <th><i class="fas '.$updown.'"></i></th>
                    </tr>
                    ');
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="container">
                <div class="card-title">Top Artists</div>
                <table class="table">
                    <tbody>
                    <?php
                    $a = 0;
                    while ($a < 10){
                        $artist_id = $sTrend_a[0][$a]['key'];
                        $dPlay = $sTrend_t[$a]['doc_count'];
                        $infos = getArtistID($artist_id);
                        $artist = $infos['artist'];
                        $updown = getUpDown($a, $artist_id, $sTrendingMinus1_a);
                        ++$a;
                        $class = getClassRank($a);
                        print('
                    <tr class="'.$class[0].'">
                        <th>
                            <span class="'.$class[1].'"> '.$a.' </span>
                        </th>
                        <th> <a href="./items.php?artist_id='.$artist_id.'">'.$artist.'</a></th>
                        <th><i class="fas '.$updown.'"></i></th>
                    </tr>
                    ');
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
