<?php
    // Swano Corp 2018 | Track of the week
    // Track of the week
    $t_otw = getTAofWeek('track_id');
    $track_infos = getTrackID($t_otw['key']);
    $t_prog = getProgress('track_id', $track_infos['track_id']);
    $t_cover = json_decode(getSpTracks($track_infos['s_track_id']),true)['album']['images'][1]['url'];

    // Artist of the week
    $a_otw = getTAofWeek('artist_id');
    $artist_infos = getArtistID($a_otw['key']);
    $a_prog = getProgress('artist_id', $artist_infos['artist_id']);
    $a_cover = json_decode(getSpArtist($artist_infos['s_artist_id']), true)['images'][1]['url'];
?>

<div class="slim-pageheader">
    <ol class="breadcrumb slim-breadcrumb"></ol>
    <h6 class="slim-pagetitle air-text-clear">Top </h6>
</div>
<div class="section-wrapper">
    <div class="row">
        <div class="col-5 bd bd-2">
            <div class="card-body-title text-center" style="padding-top: 10px">Track of the Week</div>
            <div class="row air-box">
                <div class="col-xl-7">
                    <ul>
                        <li class="air-list">
                            <i class="fas fa-music"></i>
                            <a href="./items.php?track_id=<?=$track_infos['track_id']?>">
                                <?=$track_infos['track']?>
                            </a>
                        </li>
                        <li class="air-list font-italic">
                            <i class="fas fa-user-circle"></i>
                            <a href="./items.php?artist_id=<?=$track_infos['artist_id']?>">
                                <?=$track_infos['artist']?>
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li class="air-list">
                            <i class="fas fa-chart-line"></i>
                            <span class="text-<?php print $t_prog[1].'">'.$t_prog[2].$t_prog[0]?>%</span>
                        </li>
                        <li class="air-list">
                            <i class="fa fa-sync fa-spin"></i>
                            <span class="text-info"><?=$t_otw['doc_count']?></span>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-5 text-center">
                    <a href="./items.php?track_id=<?=$track_infos['track_id']?>">
                        <img class="rounded-10" width="85%" alt="<?=$track_infos['track']?>" src="<?=$t_cover?>">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
        <div class="col-5 bd bd-2 ">
            <div class="card-body-title text-center" style="padding-top: 10px">Artist of the Week</div>
            <div class="row air-box">
                <div class="col-xl-5 text-center">
                    <a href="./items.php?artist_id=<?=$track_infos['artist_id']?>">
                        <img class="rounded-circle" width="85%" alt="<?=$artist_infos['artist']?>" src="<?=$a_cover?>">
                    </a>
                </div>
                <div class="col-xl-7">
                    <ul>
                        <li class="air-list air-text-top">
                            <i class="fas fa-user-circle"></i>
                            <a href="./items.php?artist_id=<?=$artist_infos['artist_id']?>">
                                <?=$artist_infos['artist']?>
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li class="air-list">
                            <i class="fas fa-chart-line"></i>
                            <span class="text-<?php print $t_prog[1].'">'.$t_prog[2].$t_prog[0]?>%</span>
                        </li>
                        <li class="air-list">
                            <i class="fa fa-sync fa-spin"></i>
                            <span class="text-info"><?=$a_otw['doc_count']?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>