<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 26/05/2018
 * Time: 21:39
 */

?>
<div class="slim-pageheader"></div>
            <div class="section-wrapper">
                <div class="container table-responsive" >
                    <table class="table" >
                        <div class="card-title">Top Tracks</div>
                        <tbody>
                        <?php
                        $a = 0;
                        foreach ($station_tracks as $station_track){
                            $infos = getTrackID($station_track['key']);
                            ++$a;
                            $class = getClassRank($a);
                            print '
                            <tr class="'.$class[0].'">
                                <th>'.$a.'</th>
                                <th><a href="./items.php?track_id='.$infos['track_id'].'">'.$infos['track'].' </a>
                                <a class="text-muted" href="./items.php?artist_id='.$infos['artist_id'].'">'.$infos['artist'].'</a></th>
                                <th>'.$station_track['doc_count'].'</th>
                            </tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
<div class="slim-pageheader"></div>
            <div class="section-wrapper">
                <div class="container table-responsive" >
                    <table class="table" >
                        <div class="card-title">Top Artists</div>
                        <tbody>
                        <?php
                        $a = 0;
                        foreach ($station_artists as $station_artist){
                            $infos = getArtistID($station_artist['key']);
                            ++$a;
                            $class = getClassRank($a);
                            print '
                            <tr class="'.$class[0].'">
                                <th>'.$a.'</th>
                                <th><a href="./items.php?artist_id='.$infos['artist_id'].'">'.$infos['artist'].'</a></th>
                                <th>'.$station_artist['doc_count'].'</th>
                            </tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
