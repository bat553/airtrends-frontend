<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 26/05/2018
 * Time: 20:58
 */
// swano Corp 2018 | Affiche le tableau des top titres de l'artiste.

?>

<div class="slim-pageheader"></div>
<div class="section-wrapper">
    <div class="container table-responsive">
        <table class="table">
            <div class="card-title"><h3><?=$infos['artist']?></h3><span class="text-muted">Top Tracks</span></div>
                <tbody>
                <?php
                    foreach ($top_tracks as $top_track){
                        $infos = getTrackID($top_track['key']);
                        print '<tr><th><a href="./items.php?track_id='.$infos['track_id'].'">'.$infos['track'].'</a></th><th>'.$top_track['doc_count'].'</th></tr>';
                    }
                ?>
                </tbody>
        </table>
    </div>
</div>
