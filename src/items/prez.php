<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 20/05/2018
 * Time: 23:04
 */

// Swano Corp 2018
// Affichage cadre présentation des items
$sNote = $oTop5K[1];

?>


<div class="slim-pageheader">
    <h6 class="slim-pagetitle air-text-clear">Over the last 14 days.</h6>
</div>
<div class="section-wrapper" >
        <div class="container row bd bd-2 air-box-full">
            <div class="col-xl-4 text-center">
                <?php
                if($type == "station_id"){
                    print '<div class="thumbnail"><img class="img-thumbnail" src="'.$cover.'" alt="'.$infos['display_name'].'"></div><br>';
                }
                else if ($type == "track_id" and $_SESSION['type'] == "spotify" and (!$detect->isMobile() or !$detect->isTablet())){
                    print '<div class="thumbnail"><a href="spotify:track:'.$infos['s_track_id'].'"><img class="rounded-10 img-thumbnail" style="width: 250px;"  src="'.$cover.'" alt="'.$infos['track'].'"><div class="caption"><i class="fas fa-play"></i></div></a></div><br>';
                }
                else if ($type == "track_id" and $_SESSION['type'] == "deezer"){
                    print '<div class="thumbnail"><a href="https://www.deezer.com/open_app?page=track%2F'.$infos['d_track_id'].'%3FshowLyrics%3Dfalse"><img class="rounded-10 img-thumbnail" style="width: 250px;"  src="'.$cover.'" alt="'.$infos['track'].'"><div class="caption"><i class="fas fa-play"></i></div></a></div><br>';
                }
                else if($type == "track_id"){
                    print '<div class="thumbnail"><img class="rounded-10 img-thumbnail" style="width: 250px;"  src="'.$cover.'" alt="'.$infos['track'].'"></div><br>';
                }
                if($type == "artist_id"){
                    print '<div class="thumbnail"><img class="rounded-circle img-thumbnail" style="width: 250px"  src="'.$cover.'" alt="'.$infos['artist'].'"></div><br>';
                }
                ?>
            </div>
            <div class="col-xl-1"></div>
            <div class="col-xl-5">
                <ul>
                    <?php if ($type == 'track_id'){
                        print '<li class="air-list">
                                    <i class="fas fa-music"></i>
                                    '.$infos['track'].'
                                </li>';
                    }
                    if ($type === 'track_id' or $type === 'artist_id'){
                        print '
                     <li class="air-list air-text-top">
                        <i class="fas fa-user-circle"></i>
                        <a href="./items.php?artist_id='.$infos['artist_id'].'">'.$infos['artist'].'</a>
                    </li>
                    <li class="air-list">
                        <i class="fas fa-star"></i>
                         <span class="text-'.$sNote[1].'">'.$sNote[0].'</span>
                    </li>';
                    }
                    if ($type === 'station_id'){
                        print '
                        <li class="air-list">
                                    <i class="fas fa-broadcast-tower"></i>
                                    '.$infos['display_name'].'
                        </li>';
                    }?>

                </ul>
                <ul>
                <?php
                if($type === 'track_id' or $type === 'artist_id'){
                    print '
                <li class="air-list">
                    <i class="fas fa-chart-line"></i>
                    <span class="text-'.$prog[1].'">'.$prog[2].$prog[0].'%</span>
                </li>';
                }
                else if ($type === 'station_id'){
                    print'                
                <li class="air-list">
                    <i class="fas fa-star"></i>
                    <span class="text-'.$note[0][1].'">  '.$note[0][0].'</span>
                </li>';

                }

                ?>
                <li class="air-list">
                    <i class="fa fa-sync fa-spin"></i>
                    <span class="text-info"><?=$stats?></span>
                </li>
            </ul>
            </div>
        </div>
    </div>
