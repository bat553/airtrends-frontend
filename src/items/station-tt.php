<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 27/05/2018
 * Time: 15:38
 */
// swano Corp 2018 | Affiche le tableau des derniers titres joués.
?>

<div class="slim-pageheader"></div>
<div class="section-wrapper">
    <div class="container table-responsive">
        <table class="table">
                <div class="card-title"><h3><?=$infos['display_name']?></h3><span class="text-muted">Latest Tracks</span></div>
                <tbody>
                <?php
                foreach ($last_tracks as $last_track){
                    $date = new DateTime($last_track['_source']['timestamp']);
                    $timestamp = $date->setTimezone(new DateTimeZone('Europe/Paris'));
                    $timestamp = $date->format('H:i');
                    $infos = getTrackID($last_track['_source']['track_id']);
                    print '<tr><th><a href="./items.php?track_id='.$infos['track_id'].'">'.$infos['track'].'</a> <a class="text-muted" href="./items.php?artist_id='.$infos['artist_id'].'">'.$infos['artist'].'</a></th><th>'.$timestamp.'</th></tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>