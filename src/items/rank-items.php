<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/05/2018
 * Time: 11:53
 */

// Swano corp 2018
// classement global d'un item

$bFound = $oTop5K[0];
$sNote = $oTop5K[1];
$sArray_classement = $oTop5K[2];
$dClassement = $oTop5K[3];

?>

<div class="slim-pageheader"></div>
<div class="section-wrapper">
    <div class="container table-responsive text-center">
        <table class="table air-table-rank">
            <tbody>
            <?php
            if ($bFound == False){
                print '<tr class="air-rank-current">';
                print('This item is not yet ranked... Come back later !');
                print '</tr>';
            }else {
                if ($sArray_classement[1]) {
                    print ' <tr class="air-rank-up">';
                    $sInfos = getItemId($type, $sArray_classement[1]);
                    print(' <th>' . ($dClassement - 2) . '</th><th><a href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ' );
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if (isset($sArray_classement[2])) {
                    print ' <tr class="air-rank-up">';
                    $sInfos = getItemId($type, $sArray_classement[2]);
                    print(' <th>' . ($dClassement - 1) . '</th><th><a href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if (isset($sArray_classement[3])) {
                    print ' <tr class="air-rank-up">';
                    $sInfos = getItemId($type, $sArray_classement[3]);
                    print(' <th>' . ($dClassement) . '</th><th><a href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if (isset($sArray_classement[0])) {
                    print ('<tr class="air-rank-current">');
                    $sInfos = getItemId($type, $sArray_classement[0]);
                    print(' <th><span style="color: #FFFFFF;"> ' . ($dClassement + 1) . '</span></th><th><a class="air-current-track" href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="air-current-artist" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if (isset($sArray_classement[6]) && $dClassement < $oTop5K[4]) {
                    print '<tr class="air-rank-down">';
                    $sInfos = getItemId($type, $sArray_classement[6]);
                    print(' <th>' . ($dClassement + 2) . '</th><th><a href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if(isset($sArray_classement[5]) && $dClassement < $oTop5K[4]) {
                    print '<tr class="air-rank-down">';
                    $sInfos = getItemId($type, $sArray_classement[5]);
                    print(' <th>' . ($dClassement + 3) . '</th><th><a href="./items.php?'.$type.'=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
                if(isset($sArray_classement[4])&&  $dClassement < $oTop5K[4]) {
                    print '<tr class="air-rank-down">';
                    $sInfos = getItemId($type, $sArray_classement[4]);
                    print('<th>' . ($dClassement + 4) . '</th><th><a href="./items.php?' . $type . '=' . $sInfos[$type] . '" > ' . $sInfos[$type2] . '</a> ');
                    if ($type == 'track_id'){print '<a class="text-muted text-capitalize" href="./items.php?artist_id='.$sInfos['artist_id'].'">'.$sInfos['artist'].'</a></th>';}
                    print ' </tr>';
                }
            } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="slim-pageheader"></div>
<div class="section-wrapper">
                <div class="container table-responsive" >
                    <table class="table" >
                        <div class="card-title">Stations Rank</div>
                        <tbody>
                        <?php
                        $a = 0;
                        while ($a < count($stations_stats)){
                            $station_id = $stations_stats[$a]['key'];
                            $play = $stations_stats[$a]['doc_count'];
                            $s_infos = getRadioInfos($station_id);
                            get404($s_infos);
                            $station_name = $s_infos['display_name'];
                            ++$a;
                            $class = getClassRank($a);
                            print '                            
                            <tr class="'.$class[0].'">
                                <th>'.$a.'</th>
                                <th><a href="./items.php?station_id='.$station_id.'">'.$station_name.'</a></th>
                                <th>'.$play.'</th>
                            </tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
