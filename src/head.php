<?php
// Swano corp 2018 | head.php
$dStartTime = getLoadTime();

if(!isset($title)){
    $title = "AirTrends ".$sVersion;
}
else {
    $title = $title." | AirTrends ".$sVersion;
}
if(!isset($desc)){
    $desc = "AirTrends station analytics, trends analytics, music analytics";
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title><?=$title?></title>
    <meta name="description" content="<?=$desc?>'">


    <link rel="apple-touch-icon" sizes="180x180" href="https://static.onairtrends.org/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://static.onairtrends.org/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://static.onairtrends.org/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://static.onairtrends.org/favicon/site.webmanifest">
    <link rel="shortcut icon" href="https://static.onairtrends.org/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="https://static.onairtrends.org/favicon/browserconfig.xml">
    <meta name="theme-color" content="#11C0E9">


    <!-- Slim CSS -->
    <link rel="stylesheet" href="https://static.onairtrends.org/css/slim.min.css">

    <!-- Structured Data -->
    <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "WebSite",
          "url": "https://onairtrends.org/",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "https://onairtrends.org/search/?q={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }
    </script>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Course",
      "name": "<?=$title?>",
      "description": "<?=$desc?>",
      "provider": {
        "@type": "Organization",
        "name": "AirTrends <?=$sVersion?>",
        "sameAs": "https://onairtrends.org"
      }
    }
    </script>
    <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "url": "https://onairtrends.org",
          "logo": "https://static.onairtrends.org/img/logo-airtrends.png"
        }
    </script>

</head>
<body class="air-background">
<div class="container" style="padding-top: 3%">
    <div class="row">
        <div class="col-xl-2">
            <h2 class="airtrends-logo"><a href="https://onairtrends.org/index.php">AIRTRENDS</a></h2>
        </div>
        <div class="col-xl-8"></div>
        <div class="col-xl-2">
            <?php
            if (isLogged()[0]){
                $oMe = $_SESSION['me'];
                if(isLogged()[1] == 'Spotify'){
                    $sButton = '
                    <div class="dropdown dropdown-c">
                        <a href="#" class="logged-user text-white" data-toggle="dropdown">
                            <img src="'. getProfilePic("spotify", "white") .'" alt="'. $oMe->id .'">
                            <span>'. $oMe->id .'</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <nav class="nav">
                                <a href="./profile.php" class="nav-link"><i class="fas fa-user-circle"></i> View Profile</a>
                                <a href="./oauth/logout.php" class="nav-link"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
                            </nav>
                        </div>
                    </div>';
                }
                if(isLogged()[1] == 'Deezer'){
                    $sButton ='                    
                    <div class="dropdown dropdown-c">
                        <a href="#" class="logged-user text-white" data-toggle="dropdown">
                            <img src="'. getProfilePic("deezer") .'" alt="' . $oMe->name . '">
                            <span>'. $oMe->name .'</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <nav class="nav">
                                <a href="./profile.php" class="nav-link"><i class="fas fa-user-circle"></i> View Profile</a>
                                <a href="./oauth/logout.php" class="nav-link"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
                            </nav>
                        </div>
                    </div>';

                }

            }
            else {
                $sButton = '                    
                    <div class="dropdown dropdown-c text-right">
                        <a href="#" class="text-white" data-toggle="dropdown">
                            <i class="fas fa-user-alt"></i>
                            <span>Login</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <nav class="nav">
                                <a href="./oauth/spotify.php" class="nav-link"><i class="fab fa-spotify"></i> Spotify</a>
                                <a href="./oauth/deezer.php" class="nav-link"><i class="fas fa-music"></i> Deezer</a>
                            </nav>
                        </div>
                    </div>';
            }

            print $sButton;
            ?>



        </div>
    </div>
</div>
</div>
<div class=" air-backgound-box">
    <div class="container">
        <div class="slim-pageheader"></div>
        <div class="section-wrapper">
            <div class="text-capitalize text-center">
                <h1>Discover the hidden part of the music industries</h1>
            </div>
            <div class="section-wrapper">
                <div class="row">
                    <div class="col-lg-4">
                        <form action="https://onairtrends.org/items.php" method="get" id="s_station">
                            <div class="input-group" >
                                <select id="station-list" class="form-control select2-show-search" name="station_id" onchange="this.form.submit()"  form="s_station">
                                    <option></option>
                                    <?php
                                    $sql = $oBDD->prepare('SELECT * FROM radiostats.radios ORDER BY name ASC');
                                    $sql->execute();
                                    $result = $sql->fetchall();
                                    foreach ($result as $station){
                                        print '<option value ="'.$station['station_id'].'">'.$station['display_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <form action="https://onairtrends.org/search/"  >
                            <div class="input-group mb-3">
                                <input class="form-control" placeholder="Looking for a Track?" aria-label="Search a Track?" name="track" type="text">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <form action="https://onairtrends.org/search/">
                            <div class="input-group mb-3">
                                <input class="form-control" placeholder="Or an Artist?" aria-label="Or an Artist?" name="artist" type="text">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>