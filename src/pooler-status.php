<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 20/05/2018
 * Time: 18:43
 */

?>


<div class="slim-pageheader">
    <ol class="breadcrumb slim-breadcrumb"></ol>
    <h6 class="slim-pagetitle air-text-clear">Pooler Status (Last 24 hours)</h6>
</div>
<div class="section-wrapper">
    <div class="container col-xl-12">
        <a href="./lastplayed.php">
            <button class="btn btn-oblong btn-outline-air  btn-block mg-b-10">Track timeline</button>
        </a>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div id="pool-hist"></div>
        </div>
        <div class="col-xl-6">
            <div id="pool-data"></div>
        </div>
    </div>
</div>

