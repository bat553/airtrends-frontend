<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 20:30
 */
require __DIR__.'/../vendor/autoload.php';

// Test connectivité Backend
try {

    $oCh1= curl_init("http://10.8.0.2:9400/maintenance.json");
    curl_setopt($oCh1, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($oCh1, CURLOPT_USERPWD, "frontend01:Swano74370!");
    curl_setopt($oCh1, CURLOPT_TIMEOUT, 3);
    $oResponse1 = curl_exec($oCh1);


    $oCh2 = curl_init("http://10.8.0.2:9200/air-entry/_doc/EbTOrGMBQL0nkiy560Nr");
    curl_setopt($oCh2, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($oCh2, CURLOPT_TIMEOUT, 3);
    $oResponse2 = curl_exec($oCh2);

    $sCode1 = json_decode($oResponse1, true);
    $sCode2 = json_decode($oResponse2, true);

    $sMsg = $oResponse1.' - '.$sCode2['_id'];
    if(!$oResponse1 || !$oResponse2 ){
        $bMaintenance = 'true';
        $sE = "Connexion failed!";
    }
    elseif (!$sCode2['_id']){
        $bMaintenance = 'true';
        $sE = 'ELK unreachable';
    }
    elseif($sCode1['maintenance'] == "false"){
        unset($bMaintenance);
    }
    else {
        $bMaintenance = 'true';
        $sE = 'forced';
    }

}
catch (Exception $sE){
    $bMaintenance = 'true';
    $sE = "Connexion failed!";
}
if(isset($bMaintenance)){
    header(http_response_code(500));
    print'
    <link href="https://static.onairtrends.org/css/bootstrap.min.css" rel="stylesheet">
    <error>
    <title>Error 500 !</title>
    <div class="alert alert-danger">
          <strong>Oh no. (500)</strong> Une maintenance doit être en cours. Réessayez dans 5 minutes.  
    </div>
    <img style="height: 100%; width: 100%" src="http://i0.kym-cdn.com/photos/images/original/001/120/813/271.gif"/>
    '.$sE.' '.$sMsg.'
    </error>';
    exit(1);
}

$sVersion = "v2.0"; // Numero de version du frontend

// SETUP elasticsearch

$sHosts = [
    'host' => '10.8.0.2',
    'port' => '9200',
    'user' => 'elastic',
    'pass' => 'Swano74370!'
];
$oClient = Elasticsearch\ClientBuilder::create()
    ->setHosts($sHosts)
    ->build();

// Setup deezer
require_once 'class.deezerapi.php';
$sDzConfig = array(
    'app_id' => "258142",
    'app_secret' => "e573242dba4746ccf8c6a8c699a836fb",
    'my_url' => "http://swla.be/oauth/deezer.php"
);
$oDzApi = new deezerapi($sDzConfig);

// Setup Spotify basic
$sClient_id = '2e3f51b377f14a6da4c2e2eb3fff4fc1';
$sClient_secret = '3ed4f236d2bc47059e03038a241ae94f';

$oCh = curl_init();
curl_setopt($oCh, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
curl_setopt($oCh, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($oCh, CURLOPT_POST,           1 );
curl_setopt($oCh, CURLOPT_POSTFIELDS,     'grant_type=client_credentials' );
curl_setopt($oCh, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($sClient_id.':'.$sClient_secret)));

$oResult=curl_exec($oCh);
$oResult = json_decode($oResult);


$sSpApiKey = $oResult->access_token; // should match with Server key
$sHeaders = array(
    'Authorization: Bearer '.$sSpApiKey
);

// Setup MySQL

$sHost = '10.8.0.2';
$sDB = 'airtrends_prod';
$sUser = 'swano';
$sPass = 'Swano74370!';
$oBDD = new PDO('mysql:host='.$sHost.';dbname='.$sDB.';charset=utf8', $sUser, $sPass);
array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

// Start Session
session_start();

if (!stripos($_SERVER["REQUEST_URI"], 'oauth')) {$_SESSION['url'] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";}
if (stripos($_SERVER["REQUEST_URI"], 'login.php')) {$_SESSION['url'] = "https://$_SERVER[HTTP_HOST]/index.php";}
if (stripos($_SERVER["REQUEST_URI"], 'profile.php')) {$_SESSION['url'] = "https://$_SERVER[HTTP_HOST]/index.php";}
if (isLogged()[0]){
    getSessionExpire();
}
// Setup Mobile detect
$detect = new Mobile_Detect;

function getCurrentPlay()
{
    global $oClient;
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => [
            'query' => [
                "match_all" => (object)[]
            ],
            "size" => 2,
            "sort" => [
                "date" => [
                    "order" => "desc"
                ]
            ]
        ]
    ];
    $sResults = $oClient->search($sParams);
    $dTrack_id0 = $sResults['hits']['hits'][0]['_source']['track_id'];
    $dTrack_id1 = $sResults['hits']['hits'][1]['_source']['track_id'];
    $sRadio0 = ucwords($sResults['hits']['hits'][0]['_source']['radio']);
    $sRadio1 = ucwords($sResults['hits']['hits'][1]['_source']['radio']);
    $sDate0 = $sResults['hits']['hits'][0]['_source']['date'];
    $sDate1 = $sResults['hits']['hits'][1]['_source']['date'];

    $sInfos0 = getTrackID($dTrack_id0);
    $sInfos1 = getTrackID($dTrack_id1);
    return array($sInfos0, setDateFormat($sDate0), $sRadio0, $sInfos1, setDateFormat($sDate1), $sRadio1);


}

function setDateFormat($date)
{
    $date = date_create($date);
    $date = date_format($date, 'H:i:s');

    return $date;
}

function getArtistID($artist_id)
{
    if(!preg_match("/^2[0-9]{7}$/", $artist_id)){
        return;
    }
    global $oClient;
    $sParams = [
        'index' => 'air-link',
        'body' => [
            'query' => [
                'match' => [
                    'artist_id' => $artist_id
                ]
            ]
        ],
    ];
    $sResults = $oClient->search($sParams);
    return $sResults['hits']['hits'][0]['_source'];
}

function getTrackID($track_id)
{
    if(!preg_match("/^1[0-9]{7}$/", $track_id)){
        return;
    }
    global $oClient;
    $sParams = [
        'index' => 'air-link',
        'body' => [
            'query' => [
                'match' => [
                    'track_id' => $track_id
                ]
            ]
        ],
    ];
    $sResults = $oClient->search($sParams);
    if (!$sResults['hits']['total']){return;}
    return $sResults['hits']['hits'][0]['_source'];
}

function getRadioInfos($station_id)
{
    global $oBDD;
    $oReq = $oBDD->prepare("SELECT * FROM radios WHERE station_id=:station_id");
    $oReq->execute(array(
        ":station_id" => $station_id
    ));
    $sResult = $oReq->fetch();
    if(!$sResult){
        return 404;
    }
    return $sResult;

}

function getRadioNote($station_id){

    global $oClient;
    $sJson = '{
    "size":0,
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "timestamp",
             "format": "YYYYMMddHHmmss",
              "ranges": [
                  {"from": "now-1M/M",
                   "to": "now"
                   }
                 ]
           },
          "aggs": {
            "_doc":{
            "terms": {
              "field": "station_id",
              "size": 150
            }
          }
          }
        }
      }
    }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    $sResults = $sResults['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];
    $sClassement = array_search($station_id, array_column($sResults, 'key'));
    $dPercent = ($sResults[$sClassement]['doc_count'] / get1MonthTotal()) * 100;
    $dPercent = round($dPercent);

    return array(getNote($sResults, $sClassement), $dPercent);


}

function get1MonthTotal(){
    global $oClient;

    $sJson = '{
      "size": 0,
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "timestamp",
            "format": "YYYYMMddHHmmss",
            "ranges": [
              {
                "from": "now-1M/M",
                "to": "now"
              }
            ]
          },
          "aggs": {
            "_doc": {
              "value_count": {
                "field": "track_id"
              }
            }
          }
        }
      }
    }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    return $sResults['aggregations']['_doc']['buckets'][0]['_doc']['value'];

}


function getNote($array, $class){

    $end = count($array);
    if($class <= ($end * 0.1)){

        $note = array("A+", "success");
    }
    elseif($class <= ($end * 0.2)){

        $note = array("A", "#2f892f");
    }
    elseif($class <= ($end * 0.3)){

        $note = array("B+", "#0d740d");
    }
    elseif($class <= ($end * 0.4)){

        $note = array("B", "#849f1a");
    }
    elseif($class <= ($end * 0.5)){

        $note = array("C+", "#bfa519");
    }
    elseif($class <= ($end * 0.6)){

        $note = array("C", "#ffd700");
    }
    elseif($class <= ($end * 0.7)){

        $note = array("D+", "#c84f0e");
    }
    elseif($class <= ($end * 0.8)){

        $note = array("D", "#e41010");
    }
    elseif($class <= ($end * 0.9)){

        $note = array("D-", "red");
    }
    else{

        $note = array("Non noté", "#625a5a");

    }

    return $note;

}

function getTrending($type)
{
    global $oClient;
    $sJson2w = '
           {
      "size": 0,
      "query": {
        "range": {
          "timestamp": {
            "gte": "now-2w/w",
            "lte": "now-1w/w"
          }
        }
      },
      "aggs": {
        "_doc": {
          "terms": {
            "field": "'.$type.'",
            "size": 10
          }
        }
      }
    }';
    $sJson1w ='
       {
  "size":0,
  "query": {
    "range": {
      "timestamp": {
        "gte": "now-1w/w",
        "lte": "now"
      }
    }
  },
  "aggs": {
    "_doc": {
      "terms": {
        "field": "'.$type.'",
        "size": 10
      }
    }
  }
}';

    $sParams2w = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson2w)
    ];
    $sResults2w = $oClient->search($sParams2w);

    $sParams1w = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson1w)
    ];
    $sResults1w = $oClient->search($sParams1w);


    $all = array(
        $sResults1w['aggregations']['_doc']['buckets'],
        $sResults2w['aggregations']['_doc']['buckets']
    );
    return $all;
}

function getUpDown($a, $track_id, $array)
{

    if ($array) {
        //print_r(json_encode($array));

        $old = array_search($track_id, array_column($array, 'key'));
        if ($old || $old == 0) {
            $updown = $old - $a;
             //print($track_id.' '.$old.' '.$a.' - ');
        } else {
            $updown = 'new';
            //print($track_id.'<- '.$updown.' -');
        }

        if ($updown > 0) { // positif
            $updown = 'fa-arrow-up';
        } elseif ($updown < 0) { // negatif
            $updown = 'fa-arrow-down';
        } elseif ($updown == 0) { // pas de progression
            $updown = 'fa-long-arrow-alt-right';
        }
        elseif ($updown == 'new' ) { // nouveau
            $updown = '<i class="material-icons">fiber_new</i>';
        }
    } else {
        $updown = 'fa-long-arrow-alt-left';
    }
    return $updown;

}

function getDzTrack($dDz_ID)
{
    global $oDzApi;
    $sInfos = $oDzApi->getTrack($dDz_ID);
    return json_decode(json_encode($sInfos), True);
}

function getDzArtist($dDz_art_ID)
{
    global $oDzApi;
    $sInfos = $oDzApi->getArtist($dDz_art_ID);
    return json_decode(json_encode($sInfos), True);
}

function getSpTracks($sSp_ID)
{
    global $sHeaders;
    $ch = curl_init("https://api.spotify.com/v1/tracks/$sSp_ID");
    // To save response in a variable from server, set headers;
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $sHeaders);
    // Get response
    $oResponse = curl_exec($ch);
    $dHttpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    return $oResponse;
}

function getSpArtist($sSp_ID_art)
{
    global $sHeaders;
    $ch = curl_init("https://api.spotify.com/v1/artists/$sSp_ID_art");
    // To save response in a variable from server, set headers;
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $sHeaders);
    // Get response
    $oResponse = curl_exec($ch);
    $dHttpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    return $oResponse;
}


function getTrendingStation($sType, $id)
{
    global $oClient;
    if ($sType == 'artist') {
        $sJson = '{
          "size": 0,
          "query" : {
                "match" : { "station_id" : "' . $id . '" }
            },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "timestamp",
                 "format": "YYYYMMddHHmmss",
                  "ranges": [
                      {"from": "now-14d/d",
                       "to": "now+1h"
                       }
                     ]
               },
              "aggs": {
                "_doc":{
                "terms": {
                  "field": "artist_id",
                  "size" : 10
                }
              }
              }
            }
          }
        }';
    }
    else if ($sType == 'track') {
        $sJson = '{
          "size": 0,
          "query" : {
                "match" : { "station_id" : "' . $id . '" }
            },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "timestamp",
                 "format": "YYYYMMddHHmmss",
                  "ranges": [
                      {"from": "now-14d/d",
                       "to": "now+1h"
                       }
                     ]
               },
              "aggs": {
                "_doc":{
                "terms": {
                  "field": "track_id",
                  "size" : 10
                }
              }
              }
            }
          }
        }';
    }
    else {
        print('error');
        exit(2);
    }
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);

    return $sResults['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];
}

function getTop5KTracks()
{
        $oCh = curl_init( "http://10.8.0.2:9400/dev-top5000.json");
        curl_setopt($oCh, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCh, CURLOPT_USERPWD, "frontend01:Swano74370!");
        $oResponse = curl_exec($oCh);
    return $oResponse;
}

function getTop5KArtists()
{
    $oCh = curl_init("http://10.8.0.2:9400/dev-top5000-art.json");
    curl_setopt($oCh, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($oCh, CURLOPT_USERPWD, "frontend01:Swano74370!");
    $oResponse = curl_exec($oCh);
    return $oResponse;
}

function getTrackTopStation($dTrack_id)
{
    global $oClient;
    $sJson = '{
              "size": 0,
              "query" : {
                    "match" : { "track_id" : "' . $dTrack_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "timestamp",
                    "format": "YYYYMMddHHmmss",
                    "ranges": [
                      {"from": "now-2w/w",
                        "to": "now"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "station_id",
                      "size" : 10
                    }
                  }
                  }
                }
              }
            }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    return $sResults['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];


}

function getArtistTopStation($artist_id)
{
    global $oClient;
    $sJson = '{
              "size": 0,
              "query" : {
                    "match" : { "artist_id" : "' . $artist_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "timestamp",
                    "format": "YYYYMMddHHmmss",
                    "ranges": [
                      {"from": "now-2w/w",
                        "to": "now"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "station_id",
                      "size" : 10
                    }
                  }
                  }
                }
              }
            }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    return $sResults['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];


}

function getArtistTopTracks($artist_id){
    global $oClient;
    $sJson = '{
              "size": 0,
              "query" : {
                    "match" : { "artist_id" : "' . $artist_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "timestamp",
                    "format": "YYYYMMddHHmmss",
                    "ranges": [
                      {"from": "now-14d/d",
                        "to": "now+1h"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "track_id",
                      "size" : 10
                    }
                  }
                  }
                }
              }
            }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    return $sResults['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];



}

/*function getPlayTime($sType, $ID){
    global $oClient;
    $ID = strtolower($ID);
    if ($sType == 'artiste') {
        $sJson = '{
          "size" : 0,
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k",
                "keyed" : true
              }
            }
          },
          "query": {
            "match": {
              "artist_id": "'.$ID.'"
            }
          }
        }';
    }
    else if ($sType == 'morceau') {
        $sJson = '{
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k",
                "keyed" : true
              }
            }
          },
          "query": {
            "match": {
              "track_id": "'.$ID.'"
            }
          }
        }';
    }
    else if ($sType == 'radio') {
        $sJson = '{
          "size" :0,
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k",
                "keyed" : true
              }
            }
          },
          "query": {
            "match": {
              "radio": "'.$ID.'"
            }
          }
        }';
    }
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $sResults = $oClient->search($sParams);
    $dPlaytime = $sResults['aggregations']['_doc']['buckets'];
    $sArray_playtime = array();
    $a =0;
    foreach ($dPlaytime as $row){

        $hour = $row['key_as_string'];
        $count = $row['doc_count'];
        #if ($count == 0) {$count = NULL;}
        $array_playtime['labels'][$a] = $hour;
        $array_playtime['series'][0][$a] = $count;
        ++$a;
    }
    return json_encode($sArray_playtime);



}*/

function isLogged(){
    // Check if user is logged
    global $_SESSION;

    if (isset($_SESSION['type'])) {
        if ($_SESSION['type'] == 'spotify') {
            $sType = 'Spotify';

        } elseif ($_SESSION['type'] == 'deezer') {
            $sType = 'Deezer';

        } else {
            http_response_code(500);
            print('Error in the type of service... Exiting..');
            sleep(2);
            $_SESSION['url'] = "https://onairtrends.org/";
            header("Location: ./oauth/logout.php");
            die();

        }
        return array(True, $sType, $_SESSION['name']);
    }
    else {

        return array(False, 'guest');
    }


}

function setLogElastic($sArray){
    // Send log to elasticsearch
    global $oClient;
    $sJson = '{
        "timestamp" : "'.$sArray[0].'",
        "page" : "'.$sArray[1].'",
        "load_time" :"'.$sArray[2].'",
        "keywords" : "'.$sArray[3].'", 
        "client_ip" : "'.$sArray[4].'",
        "device" : "'.$sArray[5].'",
        "refer_address" : "'.$sArray[6].'",
        "product" : "'.$sArray[7].'",
        "username" : "'.$sArray[8].'"
    }';
    $sParams = [
        'index' => 'web-stats',
        'type' => '_doc',
        'id' => uniqid(),
        'body' => json_decode($sJson)
    ];
    //print_r($sParams);
    $oClient->index($sParams);

}


function getLoadTime(){
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        return ($time);
}

function getRadios(){

    global $oBDD;
    $oReq = $oBDD->prepare("SELECT display_name, name FROM radios ORDER BY name");
    $oReq->execute();
    $sResult = $oReq->fetchAll();

    return $sResult;
}

function getLastPlayedStation($station_id){
    global $oClient;
    $sJson = '{
      "query": {
        "match": {
          "station_id": "'.$station_id.'"
        }
      },
      "size": 7,
      "sort": [
        {
          "timestamp": {
            "order": "desc"
          }
        }
      ]
    }';

    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];

    $result = $oClient->search($sParams);
    return $result['hits']['hits'];
}

/* function getCurrentPlayed(){
    global $oBDD;
    $oReq = $oBDD->prepare("SELECT track_id, artist_id, timestamp, radio FROM entry ORDER BY timestamp DESC LIMIT 2");
    $oReq->execute();
    $sResult = $oReq->fetchAll();
    $a = 0;
    $sArray = array();
    array_push($return, $sResult[0]['radio'], $sResult[1]['radio']);
    while ($a < 2) {
        $oReq = $oBDD->prepare("SELECT artiste, morceau, track_id, artist_id FROM link WHERE track_id = :track_id AND artist_id = :artist_id");
        $oReq->execute(array(
            ":track_id" => $sResult[$a]['track_id'],
            ":artist_id" => $sResult[$a]['artist_id']
        ));
        array_push($sArray, $oReq->fetchall());
        ++$a;
    }
    return $sArray;

}*/

/* function getFirstPlay($track_id){
    global $oClient;
    $sJson = '{
      "query": {
        "match": {
          "track_id": "'.$track_id.'"
          
        }
      },
      "sort": [
        {
          "date": {
            "order": "asc"
          }
        }
      ],
      "size": 1
    }';

    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];

    $result = $oClient->search($sParams);
    return $result['hits']['hits'][0]['_source'];
}*/


function getTAofWeek($type){
    global $oClient;
    $sJson = '{
      "size": 0,
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "timestamp",
            "format": "YYYYMMddHHmmss",
            "ranges": [
              {
                "from": "now-1w/w",
                "to": "now"
              }
            ]
          },
          "aggs": {
            "_doc": {
              "terms": {
                "field": "'.$type.'",
                "size" : 1
              }
            }
          }
        }
      }
    }';

    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)

    ];
    $result = $oClient->search($sParams);
    return $result['aggregations']['_doc']['buckets'][0]['_doc']['buckets'][0];

}

function getProgress($type, $id){
    global $oClient;
    $sJsonm1 = '{
          "size": 0,
          "query": {
            "match": {
              "'.$type.'": "'.$id.'"
            }
          },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "timestamp",
                "format": "YYYYMMddHHmmss",
                "ranges": [
                  {
                    "from": "now-2w/w",
                    "to": "now-1w/w"
                  }
                ]
              }
            }
          }
        }';
    $sJsonC = '{
          "size": 0,
          "query": {
            "match": {
              "'.$type.'": "'.$id.'"
            }
          },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "timestamp",
                "format": "YYYYMMddHHmmss",
                "ranges": [
                  {
                    "from": "now-1w/w",
                    "to": "now"
                  }
                ]
              }
            }
          }
        }';
    $sParamsC = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJsonC)

    ];
    $sParamsm1 = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJsonm1)

    ];
    $resultm1 = $oClient->search($sParamsm1)['aggregations']['_doc']['buckets'][0]['doc_count'];
    $resultC = $oClient->search($sParamsC)['aggregations']['_doc']['buckets'][0]['doc_count'];
    $iProg = round(($resultC - $resultm1)/$resultm1)*100;
    if (is_nan($iProg) or  is_infinite($iProg)){ # Si pas un numéro (nouveau titre donc div par zero) alors progression de 100%
        $iProg = 100;
    }
    if($iProg < 0){$class = 'danger'; $signe = '-';}
    else {$class = 'success'; $signe = "+";}
    return array($iProg, $class, $signe);
}

function getItemStats($type, $id){
    global $oClient;
    $sJson  = '{
      "size":0,
      "query": {
        "match": {
          "'.$type.'": "'.$id.'"
        }
      },
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "timestamp",
            "ranges": [
              {
                "from": "now-3w/w",
                "to": "now"
              }
            ]
          }
        }
      }
    }';
    $sParams = [
        'index' => 'air-entry',
        'type' => '_doc',
        'body' => json_decode($sJson)
    ];
    $result = $oClient->search($sParams);
    return $result['aggregations']['_doc']['buckets'][0]['doc_count'];
}

function get404($data){
    if(!$data or $data === 404){
        http_response_code(404);
        require_once "404.html";
        exit(1);
    }else{
        return;
    }

}

function getClassRank($a){
    if ($a <= 3){
        $class0 = "air-top-rank";
        $class1 = "air-top-place";
    }
    else{
        $class0 ="air-rank";
        $class1 ="air-place";
    }
    return array($class0, $class1);
}

function getGlobalRank($type, $id){
    if($type == 'artist_id'){$Top5K = getTop5KArtists();}
    else if ($type == 'track_id'){$Top5K = getTop5KTracks();}
    else {print 'Error Top 5K... Exiting...'; exit(1);}
    $Top5K = json_decode($Top5K, true);

    //print_r($Top5K);
    $dClassement = array_search($id, array_column($Top5K, 'key'));
    $sArray_classement = array();
    if(is_int($dClassement)){
        $bFound = True;
        $sNote = GetNote($Top5K, $dClassement);

        array_push($sArray_classement, $id); // Ajoute le morceau en cours en position 0
        // Cherche les 3 devant lui
        $a = $dClassement - 3;
        while ($a < $dClassement) {
            if(isset($Top5K[$a]['key'])) {
                array_push($sArray_classement, $Top5K[$a]['key']);
            } else {
                array_push($sArray_classement, NULL);
            }
            ++$a;
        }
        // Cherche les 3 derieres lui
        $a = $dClassement + 3; // pas prendre celui en cours
        while ($a > $dClassement) {
            if(isset($Top5K[$a]['key'])) {
                array_push($sArray_classement, $Top5K[$a]['key']);
            } else {
                array_push($sArray_classement, NULL);
            }
            --$a;
        }

#print_r($sArray_classement);
    }
    else{
        $bFound = False;
        array_push($sArray_classement, 'notfound');
        $sNote = array("Non noté", "#625a5a");
    }

    return array($bFound, $sNote, $sArray_classement, $dClassement, count($Top5K));
}

function getItemId($type, $id){
    if(!preg_match("/^1[0-9]{7}$/", $id) and !preg_match("/^2[0-9]{7}$/", $id)){
        return;
    }
    global $oClient;
    $sParams = [
        'index' => 'air-link',
        'body' => [
            'query' => [
                'match' => [
                    $type => $id
                ]
            ]
        ],
    ];
    $sResults = $oClient->search($sParams);
    if (!$sResults['hits']['total']){return;}
    return $sResults['hits']['hits'][0]['_source'];

}

function getProfilePic($service, $color="simple"){
    global $oMe;
    global $oDzApi;
    if($service == "spotify"){
        if ($oMe->images[0]->url){
            return $oMe->images[0]->url;
        }
        elseif ($color == "white"){
            return "https://static.onairtrends.org/img/spotify-icon-w.png";
        }
        else {
            return "https://static.onairtrends.org/img/spotify-icon.png";
        }
    }
    elseif ($service == "deezer"){
        if($oMe->picture_medium){
            return $oMe->picture_medium;
        }
        else{
            return "https://static.onairtrends.org/img/generic-icon.png";
        }
    }
    else{
        return "https://static.onairtrends.org/img/generic-icon.png";
    }

}

function getSessionExpire(){
    $now = time();
    if($now > $_SESSION['expire'] or !$_SESSION['expire']){
        header("Location: ./oauth/logout.php");
    }
}