<?php
/*
///////////////////////////////
		|Swlabe Project|
//////////////////////////////

Callback du Oauth2
*/
require_once '../src/functions.php';
error_reporting(-1);
ini_set('display_errors', 1);
require '../vendor/autoload.php';
$session = new SpotifyWebAPI\Session('2e3f51b377f14a6da4c2e2eb3fff4fc1', '3ed4f236d2bc47059e03038a241ae94f', 'https://onairtrends.org/oauth/callback.php');

$api = new SpotifyWebAPI\SpotifyWebAPI();
if (isset($_GET['code'])) {
    $session->requestAccessToken($_GET['code']);
	$refreshToken = $session->getRefreshToken();
    $_SESSION['token'] = $session->getAccessToken();
	$_SESSION['refresh'] = $refreshToken;
	$_SESSION['time'] = $session->getTokenExpiration($session->getAccessToken()); 
	$_SESSION['type'] = 'spotify';
	$accessToken = $_SESSION['token'];
	$api->setAccessToken($accessToken);
	$me = $api->me();
    $_SESSION['me'] = $me;
	// AJOUT A LA BDD
    global $oBDD;
	$email = $me->email;
    $_SESSION['name'] = $me->id;
    $_SESSION['expire'] = time() + (30 * 60);
	$req = $oBDD->prepare("SELECT email, service FROM users WHERE email=:email AND service='spotify'");
	$req->execute(array(
		":email"=> $email
	));
	$result = $req->fetchAll();
	foreach ($result as $row){
		$Remail = $row['email'];
	}
	if ($Remail == NULL){
		$country = $me->country;
		$name = $me->id;
        $_SESSION['name'] = $name;
		$email = $me->email;
		$date = date('Y-m-d H:i');
		$product = $me->product;
		try {
			$req = $oBDD->prepare("INSERT INTO `users` (`ID`, `first_connect`, `last_connect`, `name`, `email`, `country`, `service`, `product`, `refresh_token`) VALUES (NULL, :date, :date, :name, :email, :country, 'spotify', :product, :refresh_token)");
			$req->execute(array(
				":email"=> $email,
				":date" => $date,
				":name" => $name,
				":country" => $country,
				":product" => $product,
				":refresh_token" => $_SESSION['refresh']
			));
		} catch (PDOException $e) {
			echo 'Could not connect : ' . $e->getMessage();
		}
		
	
	}
	else {
		try{
		$date = date('Y-m-d H:i');
		$req = $oBDD->prepare("UPDATE `users` SET `last_connect` = :date, `refresh_token` = :refresh_token WHERE `users`.`email` = :email AND `users`.`service` = 'spotify'  ");
			$req->execute(array(
				":email"=> $email,
				":date" => $date,
				":refresh_token" => $_SESSION['refresh']
			));
		} catch (PDOException $e) {
			echo 'Could not connect : ' . $e->getMessage();
			
		}
		
	}
    print ('Welcome back '.$_SESSION['name'].'!');
    sleep(1);
    header('Location: '. $_SESSION['url'] );

} else {
    $scopes = [
    'scope' => [
    'playlist-read-private',
    'user-read-private',
	'user-read-email',
	'playlist-modify-public',
	'user-follow-read',
	'user-follow-modify',
	'user-library-modify',
	'user-library-read'

        ],
    ];
    header('Location: ' . $session->getAuthorizeUrl($scopes));
}

?>
