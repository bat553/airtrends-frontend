    <?php
    require_once '../src/functions.php';
    session_start();
    $app_id     = "258142";
    $app_secret = "e573242dba4746ccf8c6a8c699a836fb";
    $my_url     = "https://onairtrends.org/oauth/deezer.php";
     
    session_start();
    $code = $_REQUEST["code"];
     
    if(empty($code)){
    	$_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
     
    	$dialog_url = "https://connect.deezer.com/oauth/auth.php?app_id=".$app_id
    		."&redirect_uri=".urlencode($my_url)."&perms=email,offline_access"
    		."&state=". $_SESSION['state'];
     
    	header("Location: ".$dialog_url);
    	exit;
     
    	}
     
    if($_REQUEST['state'] == $_SESSION['state']) {
    	$token_url = "https://connect.deezer.com/oauth/access_token.php?app_id="
    	.$app_id."&secret="
    	.$app_secret."&code=".$code;
     
    	$response  = file_get_contents($token_url);
    	$params    = null;
    	parse_str($response, $params);
    	$api_url   = "https://api.deezer.com/user/me?access_token="
    			.$params['access_token'];
		$_SESSION['token'] = $params['access_token'];
		$token = $params['access_token'];
		$_SESSION['type'] = 'deezer';     
    	$me = json_decode(file_get_contents($api_url));
		$_SESSION['me'] = $me;
    // AJOUT A LA BDD
	global $oBDD;
	$email = $me->email;
    $_SESSION['name'] = $me->name;
    $_SESSION['expire'] = time() + (30 * 60);
    $req = $oBDD->prepare("SELECT email, service FROM users WHERE email=:email AND service='deezer'");
	$req->execute(array(
		":email"=> $email
	));
	$result = $req->fetchAll();
	foreach ($result as $row){
		$Remail = $row['email'];
	}
	if ($Remail == NULL){
		$country = $me->country;
		$name = $me->name;
		$email = $me->email;
		$date = date('Y-m-d H:i');
		try {
			$req = $oBDD->prepare("INSERT INTO `users` (`ID`, `first_connect`, `last_connect`, `name`, `email`, `country`, `service`, `refresh_token`) VALUES (NULL, :date, :date, :name, :email, :country, 'deezer', :token)");
			$req->execute(array(
				":email"=> $email,
				":date" => $date,
				":name" => $name,
				":country" => $country,
				":token" => $token
			));
		} catch (PDOException $e) {
			echo 'Could not connect : ' . $e->getMessage();
		}
		
	
	}
	else {
		try{
		$date = date('Y-m-d H:i');
		$req = $oBDD->prepare("UPDATE `users` SET `last_connect` = :date, refresh_token = :token WHERE `users`.`email` = :email AND `users`.`service` = 'deezer'  ");
			$req->execute(array(
				":email"=> $email,
				":date" => $date,
				":token" => $token
			));
		} catch (PDOException $e) {
			echo 'Could not connect : ' . $e->getMessage();
			
		}
				
	}
	header('Location: '. $_SESSION['url'] );

}
    ?>